//
//  ViewController.swift
//  BLEAccelerator
//
//  Created by Kazuya Shida on 5/25/17.
//  Copyright © 2017 UniFa Co.,Ltd. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {
    let disposeBag = DisposeBag()

    @IBOutlet weak var tableView: UITableView!

    struct Item {
        let title: String
        let subtitle: String
        let identifier: String
    }

    let items: [Item] = [
        Item(
            title: "01. Central",
            subtitle: "Peripheralの検出と取得した加速度の値を表示します",
            identifier: "Scan"
        ),
        Item(
            title: "02. Peripheral",
            subtitle: "Advertisingの開始とCentralに加速度の値を送信します",
            identifier: "Peripheral"
        )
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        Observable.just(items)
            .bind(to: tableView.rx.items(cellIdentifier: "Cell")) { (_, item: Item, cell) in
                cell.textLabel?.text = item.title
                cell.detailTextLabel?.text = item.subtitle
            }
            .addDisposableTo(disposeBag)

        tableView.rx
            .modelSelected(Item.self)
            .subscribe(onNext: { [weak self] (item) in
                self?.performSegue(withIdentifier: item.identifier, sender: nil)
            })
            .addDisposableTo(disposeBag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
