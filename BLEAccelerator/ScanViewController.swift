//
//  ScanViewController.swift
//  BLEAccelerator
//
//  Created by Kazuya Shida on 5/25/17.
//  Copyright © 2017 UniFa Co.,Ltd. All rights reserved.
//

import UIKit
import CoreBluetooth
import RxSwift
import RxCocoa

class ScanViewController: UIViewController {
    let disposeBag = DisposeBag()

    @IBOutlet weak var tableView: UITableView!

    let scanTime: Double = 5 // seconds
    var centralManager: CBCentralManager?
    fileprivate var peripherals = Variable<[CBPeripheral]>([])

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(scan), for: .valueChanged)
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        centralManager = CBCentralManager(delegate: self, queue: nil)

        peripherals.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: "Cell")) { (_, item: CBPeripheral, cell) in
                cell.textLabel?.text = item.name
                cell.detailTextLabel?.text = item.identifier.uuidString
            }
            .addDisposableTo(disposeBag)

        tableView.addSubview(refreshControl)
        tableView.rx
            .modelSelected(CBPeripheral.self)
            .subscribe(onNext: { (peripheral) in
                self.performSegue(withIdentifier: "Central", sender: peripheral.identifier)
            })
            .addDisposableTo(disposeBag)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        /// Clear old peripherals
        peripherals.value.removeAll()

        /// Start scan
        refreshControl.beginRefreshing()
        tableView.setContentOffset(
            CGPoint(x: 0, y: tableView.contentOffset.y - refreshControl.frame.height), animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            self?.refreshControl.sendActions(for: .valueChanged)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard let manager = centralManager else { return }
        if manager.isScanning {
            manager.stopScan()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func scan() {
        guard let manager = centralManager else { return }
        if !manager.isScanning {
            manager.scanForPeripherals(withServices: nil, options: nil)

            DispatchQueue.main.asyncAfter(deadline: .now() + scanTime) { [weak self] in
                manager.stopScan()
                self?.refreshControl.endRefreshing()
            }
        } else {
            refreshControl.endRefreshing()
        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? CentralViewController,
            let uuid = sender as? UUID {
            viewController.identifier = uuid.uuidString
        }
    }
}

// MARK: - CBCentralManagerDelegate

extension ScanViewController: CBCentralManagerDelegate {

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        NSLog("%@, state: %d", #function, central.state.rawValue)
    }

    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        if let name = advertisementData[CBAdvertisementDataLocalNameKey] as? String, name.lowercased().contains("ble") {
            if peripherals.value.filter({ $0 == peripheral }).isEmpty {
                peripherals.value.append(peripheral)
            }
        }
    }
}
