//
//  Acceleration.swift
//  BLEAccelerator
//
//  Created by Kazuya Shida on 5/26/17.
//  Copyright © 2017 UniFa Co.,Ltd. All rights reserved.
//

import Foundation
import CoreBluetooth
import CoreMotion

// MARK: - Acceleration

struct Acceleration {
    var x: Float
    var y: Float
    var z: Float

    init() {
        x = 0
        y = 0
        z = 0
    }

    init(acceleration: CMAcceleration) {
        x = Float(acceleration.x)
        y = Float(acceleration.y)
        z = Float(acceleration.z)
    }

    init(bytes: Data) {
        x = Data(bytes[0..<4]).to(type: Float.self)
        y = Data(bytes[4..<8]).to(type: Float.self)
        z = Data(bytes[8..<12]).to(type: Float.self)
    }

    var data: Data {
        var data = Data()
        data.append(x.bytes)
        data.append(y.bytes)
        data.append(z.bytes)
        return data
    }

    enum UUID: String {
        case service = "0011"
        case characteristic = "0012"

        var uuid: CBUUID {
            return CBUUID(string: self.rawValue)
        }
    }

    static var advertisementName: String {
        get {
            return UserDefaults.standard.object(forKey: "AdvertisementName") as? String ?? "BLE(001)"
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "AdvertisementName")
            UserDefaults.standard.synchronize()
        }
    }
}
